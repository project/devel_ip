CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Devel IP is an extension of Devel module which allows you to do debugging on production site by restricting the display of debugging message based on the IP address or the range of IP.

INSTALLATION
------------

Install the Devel IP module as you would normally install a contributed
Drupal module or you can use composer for example composer require "drupal/devel_ip". Visit https://www.drupal.org/node/1897420 for further
information

REQUIREMENTS
-------------

* Devel (https://www.drupal.org/project/devel) module is required.

CONFIGURATION
-------------

* This module requires no configuration.
* Recommend to clear Drupal cache.

MAINTAINERS
-----------

Current maintainers:
 * Sandeep Jangra - https://www.drupal.org/u/sandeep_jangra
 * Akash Kumar - https://www.drupal.org/u/akashkumarosl
