<?php

namespace Drupal\devel_ip\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure devel_ip settings for this site.
 */
class DevelRangeForm extends ConfigFormBase {
  /**
 * @var string Config settings */
  const SETTINGS = 'devel_range.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'devel_range_admin_setting';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $name_field = $form_state->get('num_of_ranges');
    $form['#tree'] = TRUE;

    // Get all form_id from form_id.settings config file.
    $range_config = \Drupal::configFactory()->get('devel_range.settings')->get('range_config');
    $form['range_fieldset'] = [
      '#type' => 'fieldset',
      '#prefix' => "<div id='range-fieldset-wrapper'>",
      '#suffix' => '</div>',
    ];
    if (empty($range_config)) {
      $count_range_config = 1;
    }
    else {
      $count_range_config = count($range_config);
    }
    if (empty($name_field)) {
      $name_field = $form_state->set('num_of_ranges', $count_range_config);
    }

    for ($i = 1; $i <= $form_state->get('num_of_ranges'); $i++) {
      if ($i % 2 == 0) {
        $class_name = 'form-range-field-even';
      }
      else {
        $class_name = 'form-range-field-odd';
      }
      $form['range_fieldset']['range_config'][$i]['init_range'] = [
        '#type' => 'textfield',
        '#title' => $this->t('IP Initial Range'),
        '#default_value' => $range_config[$i]['init_range'],
        '#prefix' => '<div class="form-range-field ' . $class_name . '">',
        '#suffix' => '</div>',
      ];
      $form['range_fieldset']['range_config'][$i]['end_range'] = [
        '#type' => 'textfield',
        '#title' => $this->t('IP End Range'),
        '#default_value' => $range_config[$i]['end_range'],
        '#prefix' => '<div class="form-range-field ' . $class_name . '">',
        '#suffix' => '</div>',
      ];
    }
    $form['range_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['range_fieldset']['actions']['add_range'] = [
      '#type' => 'submit',
      '#value' => t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => "range-fieldset-wrapper",
      ],
      '#prefix' => '<div class="range-button">',
      '#suffix' => '</div>',
    ];
    if ($form_state->get('num_of_ranges') > 1) {
      $form['range_fieldset']['actions']['remove_range'] = [
        '#type' => 'submit',
        '#value' => t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => "range-fieldset-wrapper",
        ],
        '#prefix' => '<div class="range-button">',
        '#suffix' => '</div>',
      ];
    }
    $form['#attached']['library'][] = 'devel_ip/form_range_css';
    $form_state->setCached(FALSE);
    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_of_ranges');
    return $form['range_fieldset'];
  }

  /**
   *
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_of_ranges');
    $add_button = $name_field + 1;
    $form_state->set('num_of_ranges', $add_button);
    $form_state->setRebuild();
  }

  /**
   *
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_of_ranges');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_of_ranges', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $range_config = $form_state->getValue(['range_fieldset', 'range_config']);
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('range_config', $range_config)
      ->save();

    $router_builder = \Drupal::service('router.builder');
    $router_builder->rebuild();

    parent::submitForm($form, $form_state);
  }

}
