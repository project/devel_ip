<?php

namespace Drupal\devel_ip\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RoutingAccess extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('devel_php.execute_php');
    // Validates for devel php access.
    if ($route) {
      // Config file name of devel_ip.
      $ip_config = 'devel_ip.settings';
      // Get array of IPs.
      $ip_array = \Drupal::configFactory()->get($ip_config)->get('ip');
      // Config file.
      $range_config = 'devel_range.settings';
      $range_array = \Drupal::configFactory()->get($range_config)->get('range_config');
      // Checking range with user ip address.
      $flag = 0;
      // User ip.
      $user_ip = \Drupal::request()->getClientIP();
      $route->setRequirement('_access', 'FALSE');
      if (is_array($range_array)) {
        foreach ($range_array as $key => $value) {
          $i = 0;
          foreach ($value as $k) {
            $arr[$i] = $k;
            $i++;
          }
          if ((ip2long($user_ip) >= ip2long($arr[0])) && (ip2long($user_ip) <= ip2long($arr[1]))) {
            $route->setRequirement('_access', 'TRUE');
            $flag = 1;
          }
        }
      }
      // Check whether current user ip address is in $result array.
      if ($flag == 0) {
        if ((is_array($ip_array))&&(in_array($user_ip, $ip_array))) {
          $route->setRequirement('_access', 'TRUE');
        }
        else {
          $route->setRequirement('_access', 'FALSE');
        }
      }
    }
  }

}


